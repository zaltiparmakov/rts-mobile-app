import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ParamMap } from '@angular/router/src/shared';
import { Location, DatePipe } from '@angular/common';

import { PostsService } from '../../services/posts.service';
import { Media } from '../../models/media';
import { Post } from '../../models/post';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  post: Post;

  constructor(private postsService: PostsService, private route: ActivatedRoute,
    private location: Location) { }

  ngOnInit() {
    this.getPost();
  }

  getPost(): void {
    const id = +this.route.snapshot.paramMap.get('id');

    this.postsService.getPost(id)
      .subscribe(post => {
        this.post = post;
      });
  }

  goBack(): void {
    this.location.back();
  }

  getReadTime() {
    let timeToRead;
    const words = this.post.content.rendered.split(' ').length;
    const minutes = Math.floor(words / 120);
    const seconds = Math.floor(words % 120 / (120 / 60));

    if (1 <= minutes) {
      timeToRead = minutes + ' min';
    } else {
      timeToRead = seconds + ' s';
    }

    return timeToRead;
  }
}
