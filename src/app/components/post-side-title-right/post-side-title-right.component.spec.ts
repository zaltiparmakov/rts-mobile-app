import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostSideTitleRightComponent } from './post-side-title-right.component';

describe('PostSideTitleRightComponent', () => {
  let component: PostSideTitleRightComponent;
  let fixture: ComponentFixture<PostSideTitleRightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostSideTitleRightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostSideTitleRightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
