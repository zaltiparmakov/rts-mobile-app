import { Component, OnInit, Input } from '@angular/core';
import { Post } from '../../models/post';

@Component({
  selector: 'app-post-side-title-right',
  templateUrl: './post-side-title-right.component.html',
  styleUrls: ['./post-side-title-right.component.css']
})
export class PostSideTitleRightComponent implements OnInit {

  @Input() post: Post;

  constructor() { }

  ngOnInit() {
  }

}
