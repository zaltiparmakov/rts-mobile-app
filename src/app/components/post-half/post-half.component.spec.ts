import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostHalfComponent } from './post-half.component';

describe('PostHalfComponent', () => {
  let component: PostHalfComponent;
  let fixture: ComponentFixture<PostHalfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostHalfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostHalfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
