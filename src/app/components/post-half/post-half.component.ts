import { Component, OnInit, Input } from '@angular/core';
import { Post } from '../../models/post';

@Component({
  selector: 'app-post-half',
  templateUrl: './post-half.component.html',
  styleUrls: ['./post-half.component.css']
})
export class PostHalfComponent implements OnInit {

  @Input() post: Post;

  constructor() { }

  ngOnInit() {
  }

}
