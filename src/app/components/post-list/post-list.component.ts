import { Component, OnInit } from '@angular/core';

import { Post } from '../../models/post';
import { Media } from '../../models/media';
import { PostsService } from '../../services/posts.service';
import { FeaturedPostComponent } from '../featured-post/featured-post.component';
import { Category } from '../../models/category';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {
  posts: Post[];
  media: Media;
  category: Category;

  // pagination variables
  private postsPerPage;
  private itemsToLoad = 6;
  public isFullListDisplayed = false;
  private currentPage;
  private totalPosts = 31;
  private haveMorePosts;

  private id = +this.route.snapshot.paramMap.get('id');

  constructor(private postsService: PostsService, private route: ActivatedRoute,
     private router: Router) {
       // when the route is changed (chosen different menu)
        this.route.params.subscribe((params: Params) => {
          // reset variables on every page change, for pagination to work properly
          this.postsPerPage = 6;
          this.currentPage = 2;
          this.haveMorePosts = true;
          this.isFullListDisplayed = false;

          this.getPostsByCategory(params.id, params.page);
        });
  }

  ngOnInit() {
    const page = +this.route.snapshot.paramMap.get('page');

    this.getPostsByCategory(this.id, page);
  }

  getPostsByCategory(catId: number, page: number): void {
    this.postsService.getPostsByCategory(catId, page)
    .subscribe(
      posts => this.posts = posts,
      error => console.log('No more posts to show.'),
    );
  }

  getMorePostsByCategory(catId: number, page: number): void {
    this.postsService.getPostsByCategory(catId, page)
    .subscribe(
      posts => {
        // xhr get new posts and concatenate to the array of posts
        this.posts = this.posts.concat(posts);
      },
      error => {
        // when there is no more posts
        this.haveMorePosts = false;
        this.isFullListDisplayed = true;
      },
    );
  }

  // get more posts via xhr when the page is scrolled
  onScroll() {
    if (this.posts) {
      if (this.postsPerPage <= this.totalPosts && this.haveMorePosts) {
        this.postsPerPage += this.itemsToLoad;
        this.getMorePostsByCategory(this.id, this.currentPage);
        this.currentPage++;
      } else {
        // to disable scrolling on page (used in view)
        this.isFullListDisplayed = true;
      }
    }
  }
}
