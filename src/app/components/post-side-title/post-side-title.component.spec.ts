import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostSideTitleComponent } from './post-side-title.component';

describe('PostSideTitleComponent', () => {
  let component: PostSideTitleComponent;
  let fixture: ComponentFixture<PostSideTitleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostSideTitleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostSideTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
