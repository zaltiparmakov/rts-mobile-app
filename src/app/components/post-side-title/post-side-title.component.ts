import { Component, OnInit, Input } from '@angular/core';
import { Post } from '../../models/post';

@Component({
  selector: 'app-post-side-title',
  templateUrl: './post-side-title.component.html',
  styleUrls: ['./post-side-title.component.css']
})
export class PostSideTitleComponent implements OnInit {

  @Input() post: Post;

  constructor() { }

  ngOnInit() {
  }

}
