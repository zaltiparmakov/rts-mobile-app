import { Component, OnInit } from '@angular/core';

import { Menu } from './models/menu';
import { PostsService } from './services/posts.service';

import { Router, ActivatedRoute } from '@angular/router';
import { RouterState } from '@angular/router/src/router_state';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  menu: Menu[];

  constructor(
    private postsService: PostsService,
    public router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.postsService.getMenu()
      .subscribe(menu => this.menu = menu);
  }
}
