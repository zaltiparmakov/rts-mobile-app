import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Weather } from '../models/weather';

import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';


@Injectable()
export class WeatherService {
  _rtsApi = environment.rts_api;

  constructor(private http: HttpClient) { }

  getWeatherData(): Observable<any> {
    return this.http.get(this._rtsApi + 'weather/maribor');
  }
}
