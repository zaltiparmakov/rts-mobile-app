import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

// models
import { Post } from '../models/post';
import { Observable } from 'rxjs/Observable';
import { Media } from '../models/media';
import { Menu } from '../models/menu';

@Injectable()
export class PostsService {
  _wpBase = environment.wp_api;
  _rtsApi = environment.rts_api;

  constructor(private http: HttpClient) { }

  getPost(id: number): Observable<Post> {
    return this.http.get<Post>(this._wpBase + 'posts/' + id);
  }

  getPostsByCategory(catId: number, page: number) {
    return this.http.get<Post[]>(this._rtsApi + 'category/' + catId + '/' + page);
  }

  getMenu(): Observable<Menu[]> {
    return this.http.get<Menu[]>(this._rtsApi + 'menu');
  }
}
