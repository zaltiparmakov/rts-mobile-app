export class Media {
    id: number;
    date: String;
    guid: {
        rendered: String;
    };
    source_url: String;
    type: String;
    caption: {
        rendered: String;
    };
    media_details: {
        width: number;
        height: number;
        sizes: {
            thumbnail: {
                source_url: String;
                width: number;
                height: number;
            },
            medium: {
                source_url: String;
                width: number;
                height: number;
            },
            medium_large: {
                source_url: String;
                width: number;
                height: number;
            },
            full: {
                source_url: String;
                width: number;
                height: number;
            }
        },
        image_meta: {
            credit: String;
            camera: String;
            copyright: String;
        }
    };
    post: String;
}
