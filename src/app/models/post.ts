import { Media } from '../models/media';
export class Post {
    id: number;
    date: Date;
    code: string;
    modified: Date;
    slug: string;
    status: string;
    title: {
        rendered: string;
    };
    content: {
        rendered: string;
    };
    author: number;
    author_name: string;
    excerpt: {
        rendered: string;
    };
    featured_media: number;
    comment_status: string;
    categories: Array<string>;
    category_name: string;
    tags: Array<string>;
    featured_image_url: string;
}


