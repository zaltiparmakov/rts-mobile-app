export interface Menu {
  id: number;
  order: number;
  parent: number;
  title: string;
  url: URL;
  cat_ID: string;
  object: string;
  type: string;
  type_label: string;
}
