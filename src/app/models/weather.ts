export interface Weather {
    day: string;
    temp: string;
    desc: string;
    icon: string;
}
