import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

import { EscapeHtmlPipe } from './escapehtml.pipe';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';

// Data Providers
import { WeatherService } from './services/weather.service';
import { PostsService } from './services/posts.service';

// Components
import { PostListComponent } from './components/post-list/post-list.component';
import { PostComponent } from './components/post/post.component';
import { WeatherComponent } from './components/weather/weather.component';
import { FeaturedPostComponent } from './components/featured-post/featured-post.component';
import { PostSideTitleComponent } from './components/post-side-title/post-side-title.component';
import { PostHalfComponent } from './components/post-half/post-half.component';
import { PostSideTitleRightComponent } from './components/post-side-title-right/post-side-title-right.component';

import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

@NgModule({
  declarations: [
    AppComponent,
    EscapeHtmlPipe,
    PostListComponent,
    PostComponent,
    WeatherComponent,
    FeaturedPostComponent,
    PostSideTitleComponent,
    PostHalfComponent,
    PostSideTitleRightComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    InfiniteScrollModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [ WeatherService, PostsService ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
