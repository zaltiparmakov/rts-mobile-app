export const environment = {
  production: true,
  wp_api: 'https://rts.dev.tovarnaidej.com/wp-json/wp/v2/',
  rts_api: 'https://rts.dev.tovarnaidej.com/wp-json/rts-api/'
};
