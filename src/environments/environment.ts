export const environment = {
  production: false,
  wp_api: 'https://rts.dev.tovarnaidej.com/wp-json/wp/v2/',
  rts_api: 'https://rts.dev.tovarnaidej.com/wp-json/rts-api/'
};
