#!/bin/bash

cd /home/www/rts24-app/
ng build --prod
scp -P 20022 -i /root/.ssh/tovarna_idej_rsa -r dist rts@tile.tovarnaidej.com:/home/rts/domains/m.rts.dev.tovarnaidej.com/

echo "Successfully Copied!";
